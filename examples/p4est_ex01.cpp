/**
 * @file benchmarks/p4est_benchmark_mpidynres_fixed.cpp
 * 
 * Benchmark for p4est on libmpidynres using a constant 
 * number of quadrants.
 * Quadrants: 4^10
 * Processes: increasing from 45 to 55, adding one process per timestep
 * Start with 56 processes, as one is required by libmpidynres!
 * Use 10 phases.
 * 
 */
#define SC_ENABLE_MPI 1
#define P4EST_ENABLE_MPI 1

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "../swe_solvers/src/solver/AugRie.hpp"

#include "../scenarios/SWE_Scenario.hh"
#include "../scenarios/SWE_simple_scenarios.hh"

#include "../tools/args.hh"

#include <p4est_geometry.h>
#include "p4est_extended.h"
#include "p4est_communication.h"
extern "C" {
#include "p4est_dynres.h"
#include "mpi.h"
#include "timestamps.h"
}


static int verbosity = 0;
static char outfile[256] = "";
// List for timestamps
timestamp_list_t *timestamps;

/*
 * @brief state of the simulation
 */
struct simulation_state {
    float t;                        // the current simulation time
    float t_end;                    // the time to be simulated
    float *phases;                  // an array containing all phases in time
    int num_phases;                 // the number of phases in time
    int current_phase;              // the index of the current phase
    int refinement_level;           // the number of refinements for the p4est
    SWE_Scenario *scenario;         // the simulated scenario
    int iterations;
    int rc_freq;                    // frequency of resource change checks
    clock_t time;
    double work_time;
    int work_iters;
    int enable_monitoring;
    p4est_connectivity_t *connectivity;
};


/**
 * state of a single cell, stored inside every p4est quadrant
 */
struct cell_state {
    //cell state
    double h;
    double hu;
    double hv;
    double b;

    //derivatives / updates
    double update_h;
    double update_hu;
    double update_hv;
};


/**
 * helper struct for passing the ghost layer and the timestep 
 * to the p4est_iterate function simulatanously
 */
struct ghost_and_data {
   cell_state *ghost;
   double timestep;
};


static solver::AugRie<double> riemann_solver;

//////////////////////////////////////////////////////////////////////
////////////////////// simulation on the p4est ///////////////////////
//////////////////////////////////////////////////////////////////////


/************************************************
 *      Functions for manipulating the p4est    *
 ***********************************************/

/**
 * @brief Callback function for refining the mesh.
 */
static int refine_fn(p4est_t *p4est, p4est_topidx_t tree, 
        p4est_quadrant_t *quadrant){


    return 1;
}

/**
 * @brief initialize a cell using the scenario data
 */
static void init_cell(p4est_t *p4est, p4est_topidx_t tree, 
        p4est_quadrant_t *quadrant){

    simulation_state *sim_state = (simulation_state *) p4est->user_pointer;

    SWE_Scenario *scenario = sim_state->scenario;

    if(scenario == NULL)
        return;

    cell_state *state = (cell_state*) quadrant->p.user_data;
    float x = quadrant->x;
    float y = quadrant->y;

    double xyz[3];
    p4est_qcoord_to_vertex(p4est->connectivity, tree, x, y, xyz); 

    xyz[0] *= scenario->getBoundaryPos(BND_RIGHT) - 
                  scenario->getBoundaryPos(BND_LEFT);
    xyz[1] *= scenario->getBoundaryPos(BND_TOP) - 
                  scenario->getBoundaryPos(BND_BOTTOM);
    xyz[0] += scenario->getBoundaryPos(BND_LEFT);
    xyz[1] += scenario->getBoundaryPos(BND_BOTTOM);

    state->h = scenario->getWaterHeight(xyz[0], xyz[1]);
    state->hu = scenario->getVeloc_u(xyz[0], xyz[1]);
    state->hv = scenario->getVeloc_v(xyz[0], xyz[1]);
    state->b = scenario->getBathymetry(xyz[0], xyz[1]);

    state->update_h = 0;
    state->update_hu = 0;
    state->update_hv = 0;
}



/*****************************************
 *       Functions for simulation        *
 ****************************************/


/**
 * @brief reset all updates from previous timesteps
 */
static void resetUpdates(p4est_iter_volume_info_t * info, void *user_data){

    p4est_quadrant_t *quad = (p4est_quadrant_t *) info->quad;
    cell_state *state = (cell_state *) quad->p.user_data;
    
    state->update_h = 0;
    state->update_hu = 0;
    state->update_hv = 0;
}


/**
 * @brief compute updates for one face
 *
 * This function uses the AugRie solver to calculate updates 
 * at all faces of the tree.
 * For simplicity, the resulting updates are stored inside the quadrants.
 */
static void computeNumericalFluxes(p4est_iter_face_info_t * info, 
        void *user_data){

    simulation_state *state = (simulation_state *) info->p4est->user_pointer;

    SWE_Scenario *scenario = state->scenario;

    cell_state *ghost_data = ((ghost_and_data *) user_data)->ghost;

    sc_array_t *sides = &(info->sides);
    p4est_iter_face_side_t *first_side = 
            p4est_iter_fside_array_index_int (sides, 0);
    p4est_iter_face_side_t *second_side = 
            p4est_iter_fside_array_index_int (sides, 1);

    cell_state *first;
    cell_state *second;

    //handle hanging faces
    if(first_side->is_hanging) {
        first = new cell_state {0, 0, 0, 0, 0, 0, 0};
        
        //average over small cells
        for(int i = 0; i < P4EST_HALF; i++){
            cell_state *small_cell;

            //handle ghost layer
            if(first_side->is.hanging.is_ghost[i]) {
                small_cell = (cell_state *) &ghost_data[first_side->is.hanging.quadid[i]];
            } else {
                small_cell = (cell_state *) first_side->is.hanging.quad[i]->p.user_data;
            }

            first->h += small_cell->h;
            first->hu += small_cell->hu;
            first->hv += small_cell->hv;
            first->b += small_cell->b;
        }

        first->h /= P4EST_HALF;
        first->hu /= P4EST_HALF;
        first->hv /= P4EST_HALF;
        first->b /= P4EST_HALF;

    } else {
        //handle ghost layer
        if(first_side->is.full.is_ghost) {
            first = (cell_state *) &ghost_data[first_side->is.full.quadid];
        } else {
            first = (cell_state *) first_side->is.full.quad->p.user_data;
        }
    }

    //handle hanging faces
    if(second_side->is_hanging) {
        second = new cell_state {0, 0, 0, 0, 0, 0, 0};
        
        //average over small cells
        for(int i = 0; i < P4EST_HALF; i++){
            cell_state *small_cell;

            //handle ghost layer
            if(second_side->is.hanging.is_ghost[i]) {
                small_cell = (cell_state *) &ghost_data[second_side->is.hanging.quadid[i]];
            } else {
                small_cell = (cell_state *) second_side->is.hanging.quad[i]->p.user_data;
            }

            second->h += small_cell->h;
            second->hu += small_cell->hu;
            second->hv += small_cell->hv;
            second->b += small_cell->b;
        }

        second->h /= P4EST_HALF;
        second->hu /= P4EST_HALF;
        second->hv /= P4EST_HALF;
        second->b /= P4EST_HALF;

    } else {
        //handle ghost layer
        if(second_side->is.full.is_ghost) {
            second = (cell_state *) &ghost_data[second_side->is.full.quadid];
        } else {
            second = (cell_state *) second_side->is.full.quad->p.user_data;
        }
    }

    //ordering of the quadrants
    if(first_side->face % 2 != 0){
        std::swap(first, second);
        std::swap(first_side, second_side);
    }


    //calculate maximum timestep from the minimal cell width

    double *maxTimestep = &((ghost_and_data *) user_data)->timestep;
    int lowestLevel;
    if(first_side->is_hanging){
        if(second_side->is_hanging){
            lowestLevel = std::max(first_side->is.hanging.quad[0]->level, 
                    second_side->is.hanging.quad[0]->level);
        } else {
            lowestLevel = std::max(first_side->is.hanging.quad[0]->level, 
                    second_side->is.full.quad->level);
        }
    } else {
        if(second_side->is_hanging){
            lowestLevel = std::max(first_side->is.full.quad->level, 
                    second_side->is.hanging.quad[0]->level);
        } else {
            lowestLevel = std::max(first_side->is.full.quad->level, 
                    second_side->is.full.quad->level);
        }
    }


    // compute updates
    double update_first_h = 0;
    double update_second_h = 0;
    double current_maxWaveSpeed;

    if(first_side->face < 2){
        //x-direction

        double update_first_hu = 0;
        double update_second_hu = 0;

        riemann_solver.computeNetUpdates(first->h, second->h,
                                 first->hu, second->hu,
                                 first->b, second->b,
                                 update_first_h, update_second_h,
                                 update_first_hu, update_second_hu,
                                 current_maxWaveSpeed);

        first->update_h += update_first_h;
        second->update_h += update_second_h;
        first->update_hu += update_first_hu;
        second->update_hu += update_second_hu;

        double min_size = P4EST_QUADRANT_LEN (lowestLevel);
        min_size *= (scenario->getBoundaryPos(BND_RIGHT) - 
                        scenario->getBoundaryPos(BND_LEFT)) / 
                    (double) P4EST_ROOT_LEN;
        double current_maxTimestep = 0.4 * min_size / current_maxWaveSpeed;

        if(current_maxTimestep < *maxTimestep)
            *maxTimestep = current_maxTimestep;

        // if one face is hanging, we have to write the updates back 
        // to all of the small cells
        if(first_side->is_hanging){
            for(int i = 0; i < P4EST_HALF; i++){
                cell_state *small_cell;

                //handle ghost cells
                if(first_side->is.hanging.is_ghost[i]) {
                    small_cell = (cell_state *) &ghost_data[first_side->is.hanging.quadid[i]];
                } else {
                    small_cell = (cell_state *) first_side->is.hanging.quad[i]->p.user_data;
                }

                //write data
                small_cell->update_h += update_first_h;
                small_cell->update_hu += update_first_hu;
            }
        }

        if(second_side->is_hanging){
            for(int i = 0; i < P4EST_HALF; i++){
                cell_state *small_cell;

                //handle ghost cells
                if(second_side->is.hanging.is_ghost[i]) {
                    small_cell = (cell_state *) &ghost_data[second_side->is.hanging.quadid[i]];
                } else {
                    small_cell = (cell_state *) second_side->is.hanging.quad[i]->p.user_data;
                }

                //write data
                small_cell->update_h += update_second_h;
                small_cell->update_hu += update_second_hu;
            }
        }

    } else {
        //y-direction

        double update_first_hv = 0;
        double update_second_hv = 0;

        riemann_solver.computeNetUpdates(first->h, second->h,
                                 first->hv, second->hv,
                                 first->b, second->b,
                                 update_first_h, update_second_h,
                                 update_first_hv, update_second_hv,
                                 current_maxWaveSpeed);

        first->update_h += update_first_h;
        second->update_h += update_second_h;
        first->update_hv += update_first_hv;
        second->update_hv += update_second_hv;


        double min_size = P4EST_QUADRANT_LEN (lowestLevel);
        min_size *= (scenario->getBoundaryPos(BND_TOP) - 
                            scenario->getBoundaryPos(BND_BOTTOM)) /
                        (double) P4EST_ROOT_LEN;
        double current_maxTimestep = 0.4 * min_size / current_maxWaveSpeed;

        if(current_maxTimestep < *maxTimestep)
            *maxTimestep = current_maxTimestep;

        // if one face is hanging, we have to write the updates back to 
        // the small cells
        if(first_side->is_hanging){
            for(int i = 0; i < P4EST_HALF; i++){
                cell_state *small_cell;

                //handle ghost cells
                if(first_side->is.hanging.is_ghost[i]) {
                    small_cell = (cell_state *) &ghost_data[first_side->is.hanging.quadid[i]];
                } else {
                    small_cell = (cell_state *) first_side->is.hanging.quad[i]->p.user_data;
                }

                //write data
                small_cell->update_h += update_first_h;
                small_cell->update_hv += update_first_hv;
            }
        }

        if(second_side->is_hanging){
            for(int i = 0; i < P4EST_HALF; i++){
                cell_state *small_cell;

                //handle ghost cells
                if(second_side->is.hanging.is_ghost[i]) {
                    small_cell = (cell_state *) &ghost_data[second_side->is.hanging.quadid[i]];
                } else {
                    small_cell = (cell_state *) second_side->is.hanging.quad[i]->p.user_data;
                }

                //write data
                small_cell->update_h += update_second_h;
                small_cell->update_hv += update_second_hv;
            }
        }
    }
}

/**
 * @brief user function for packing user data
 * 
*/
int pack_data(p4est_dynres_state_t * state){
    int rc;

    simulation_state * sim_state = (simulation_state *) state->p4est->user_pointer;

    if(0 != (rc = p4est_dynres_pack_int(state, "num_phases", sim_state->num_phases))){
        return rc;
    }

    if(0 != (rc = p4est_dynres_pack_int(state, "current_phase", sim_state->current_phase + 1))){
        return rc;
    }
    if(0 != (rc = p4est_dynres_pack_float(state, "end_time", sim_state->t_end))){
        return rc;
    }
    if(0 != (rc = p4est_dynres_pack_float(state, "current_time", sim_state->t))){
        return rc;
    }

    return 0;
}

/**
 * @brief user function for unpacking user data
 * 
*/
int unpack_data(p4est_dynres_state_t * state){
    int rc;

    simulation_state * sim_state = (simulation_state *) state->p4est->user_pointer;

    if(0 != (rc = p4est_dynres_unpack_int(state, "num_phases", &sim_state->num_phases))){
        return rc;
    }
    if(0 != (rc = p4est_dynres_unpack_int(state, "current_phase", &sim_state->current_phase))){
        return rc;
    }
    if(0 != (rc = p4est_dynres_unpack_float(state, "end_time", &sim_state->t_end))){
        return rc;
    }
    if(0 != (rc = p4est_dynres_unpack_float(state, "current_time", &sim_state->t))){
        return rc;
    }

    return 0;
}


p4est_dynres_state_t * init_p4est_dpp(const char *initial_pset, simulation_state *sim_state){
    
    MPI_Info info = MPI_INFO_NULL;
    p4est_dynres_state_t *state;
    char str[64];
    
    //create p4est
    p4est_connectivity_t *conn = p4est_connectivity_new_periodic();
    sim_state->connectivity = conn;

    if(!sim_state->enable_monitoring && sim_state->rc_freq != 0){
        MPI_Info_create(&info);                                                                                                       
        MPI_Info_set(info, "model", "DefaultReplaceModel()");
        MPI_Info_set(info, "generator_key", "replace_generator");
    }    
    state = p4est_dynres_init(initial_pset, conn, sizeof(cell_state), init_cell, sim_state,
                        pack_data, unpack_data, info);
    if(NULL == state){
        return NULL;
    } 
    if(!sim_state->enable_monitoring && sim_state->rc_freq != 0){
        MPI_Info_free(&info);
    }

    if(!state->is_dynamic){
        /* Refine the initial p4est */
        for(int level = 0; level < sim_state->refinement_level; level++) {
            p4est_refine(state->p4est, 0, refine_fn, init_cell);
            p4est_balance(state->p4est, P4EST_CONNECT_FACE, init_cell);
            p4est_partition(state->p4est, 0, NULL);
        }
    }else{
        /* Partition the exisitng forrest */
        p4est_partition(state->p4est, 0, NULL);
    }

    return state;
}

/**
 * @brief apply the updates previously calculated, 
 *        and reset the updates for the next timestep
 */
static void updateUnknowns(p4est_iter_volume_info_t * info, void *user_data){

    simulation_state *sim_state = (simulation_state *) info->p4est->user_pointer;

    SWE_Scenario *scenario = sim_state->scenario;

    double *maxTimestep = (double *) user_data;
    p4est_quadrant_t *quad = (p4est_quadrant_t *) info->quad;
    cell_state *state = (cell_state *) quad->p.user_data;
    double dx = P4EST_QUADRANT_LEN (quad->level);
    dx *= (scenario->getBoundaryPos(BND_TOP) - 
                   scenario->getBoundaryPos(BND_BOTTOM)) / 
               P4EST_ROOT_LEN;

    //apply stored updates
    state->h -= *maxTimestep / dx  * state->update_h;
    state->hu -= *maxTimestep / dx * state->update_hu;
    state->hv -= *maxTimestep / dx * state->update_hv;
}

/**
 * @brief main simulation loop between two phases
 *
 * @param	tStart	time where the simulation is started
 * @param	tEnd	time of the next phase 
 * @return	actual	end time reached
 */
float simulate_interval(p4est_dynres_state_t *state) {

    p4est_t *p4est = state->p4est;
    simulation_state *sim_state = (simulation_state *) state->p4est->user_pointer;
    float t = sim_state->t;
    sim_state->time = 0;
    int iterations = 0;
    // create the ghost quadrants
    p4est_ghost_t *ghost = p4est_ghost_new(p4est, P4EST_CONNECT_FULL);

    // create space for storing the ghost data
    cell_state *ghost_data = P4EST_ALLOC(cell_state, 
            ghost->ghosts.elem_count);

    do {
       
        // reset updates
        p4est_iterate (p4est, NULL,
                NULL,
                resetUpdates,
                NULL,
                NULL);

        // synchronize the ghost data
        p4est_ghost_exchange_data(p4est, ghost, ghost_data);

        // reset maxTimestep
        double maxTimestep = 100;

        ghost_and_data *param = new ghost_and_data {ghost_data, maxTimestep};

        // compute numerical fluxes for every edge
        // updates maxTimestep
        p4est_iterate (p4est, ghost,
                (void *) param,
                NULL,
                computeNumericalFluxes,
                NULL);
        
        //collect the timestep size from all processes
        int mpiret = sc_MPI_Allreduce (&(param->timestep), &maxTimestep, 1, 
                sc_MPI_DOUBLE, sc_MPI_MIN, state->p4est->mpicomm);
        SC_CHECK_MPI (mpiret);

        // update unknowns accordingly
        p4est_iterate (p4est, NULL,
                (void *) &maxTimestep,
                updateUnknowns,
                NULL,
                NULL);

        delete param;
        t += maxTimestep;

        iterations++;
        
    } while (t <= sim_state->phases[sim_state->current_phase]);

    p4est_ghost_destroy(ghost);
    P4EST_FREE(ghost_data);
    ghost = NULL;
    ghost_data = NULL;

    // adapt the grid
    sim_state->time = 0;
    sim_state->iterations = iterations;

    return t;
}


/////////////////////////////////////////////////////////////////
////////////////////// Main functionality ///////////////////////
/////////////////////////////////////////////////////////////////


p4est_dynres_state_t * initialize(const char *pset_name, int argc, char* argv[]) {

    simulation_state *state;
    p4est_dynres_state_t * p4est_state;

    //initialize using cli parameters
    tools::Args args;

    args.addOption("simulated-time", 't', 
            "Time in seconds to be simulated, default: 50", 
            args.Required, false);
    args.addOption("num-phases", 'c', 
            "Number of phases for resource changes, default: 50", 
            args.Required, false);
    args.addOption("refinement-level", 'r', 
            "level of refinement, default: 5", 
            args.Required, false);
    args.addOption("verbosity", 'v', 
            "level of verbosiry, default: 0", 
            args.Required, false);
    args.addOption("outfile", 'o', 
            "Name of outputfile for timestamps, default: ''", 
            args.Required, false);
    args.addOption("rc-freq", 'f', 
        "Frequency of checking for resource changes (every  n-th phase), default: 1", 
        args.Required, false);
    
    args.addOption("enable-monitoring", 'm', 
        "Wether monitoring data should be sent to the RM, default: 0", 
        args.Required, false);

    tools::Args::Result ret = args.parse(argc, argv);
    switch (ret) {
        case tools::Args::Error:
            exit(1);
        case tools::Args::Help:
            exit(0);
        default:
            break; 
    }

    verbosity = args.getArgument<int>("verbosity", 0);
    strcpy(outfile, args.getArgument<std::string>("outfile", "").c_str()); 


    //initialize scenario
    SWE_Scenario *scenario = new SWE_RadialDamBreakScenario();

    /* initialize our simulation state */
    state = (simulation_state *) malloc(sizeof(simulation_state));
    state->scenario = scenario;
    state->t = 0;
    state->num_phases = args.getArgument<int>("num-phases", 50);
    state->t_end = args.getArgument<float>("simulated-time", 50);
    state->refinement_level = args.getArgument<int>("refinement-level", 5);
    state->phases = new float[state->num_phases + 1];
    state->rc_freq = args.getArgument<float>("rc-freq", 4);
    state->enable_monitoring = args.getArgument<float>("enable-monitoring", 0);
    for (int cp = 0; cp <= state->num_phases; cp++) {
        state->phases[cp] = cp * (state->t_end / state->num_phases);
    }
    state->current_phase = 0;
    state->work_time = 0;
    state->work_iters = 0;

    /* create the dynamic p4est */
    p4est_state = init_p4est_dpp(pset_name, state);
    if(NULL == p4est_state || 0 == strcmp(outfile, "") || 0 != p4est_state->p4est->mpirank){
        timestamp_list_free(&timestamps);
        disable_timestamps();
    }

    return p4est_state; 
}


void finalize(p4est_dynres_state_t *p4est_state) {
    simulation_state * sim_state = (simulation_state *) p4est_state->user_pointer;
    p4est_dynres_finalize(&p4est_state, NULL);
    delete[] sim_state->phases;
    p4est_connectivity_destroy (sim_state->connectivity);
    free(sim_state);
}


/**
 * @brief main work loop
 *
 * calls simulate_interval
 */
int simulate(p4est_dynres_state_t *p4est_state){

    
    simulation_state * sim_state = (simulation_state *) p4est_state->p4est->user_pointer;
   
    int terminate, reconfigured, rank;
    char str[256];
    MPI_Info info;
    if(verbosity > 0 && p4est_state->p4est->mpirank == 0){
        printf("P4est: Starting simulation with t_end=%f, num_phases=%d, refine_level=%d and size %d\n", sim_state->t_end, sim_state->num_phases, sim_state->refinement_level, p4est_state->p4est->mpisize);
    }

    // main simulation loop over phases
    for(; sim_state->current_phase <= sim_state->num_phases && sim_state->t <= sim_state->t_end; sim_state->current_phase++) {
        make_timestamp(timestamps, EVENT_ITER_START, p4est_state->p4est->mpisize, sim_state->current_phase);

        if(verbosity > 0 && p4est_state->p4est->mpirank == 0){
            printf("P4est: start of phase %d/%d with size %d\n", sim_state->current_phase, sim_state->num_phases, p4est_state->p4est->mpisize);
            fflush(NULL);
        }

        make_timestamp(timestamps, EVENT_WORK_START, p4est_state->p4est->mpisize, sim_state->current_phase);
        sim_state->t = simulate_interval(p4est_state);
        make_timestamp(timestamps, EVENT_WORK_END, p4est_state->p4est->mpisize, sim_state->current_phase);

        if (sim_state->enable_monitoring && p4est_state->p4est->mpirank == 0){
            long diff = get_diff(timestamps, sim_state->current_phase, sim_state->current_phase, EVENT_WORK_START, EVENT_WORK_END);
            if(0 < diff){
                sim_state->work_time += (double) diff / (double) 1000000;
                sim_state->work_iters++;
            }
        }
        
        make_timestamp(timestamps, EVENT_ADAPT_START, p4est_state->p4est->mpisize, sim_state->current_phase);
        reconfigured = 0;
        terminate = 0;
        if(sim_state->current_phase > 0 && sim_state->rc_freq != 0 && sim_state->current_phase % sim_state->rc_freq == 0){
            if (sim_state->enable_monitoring){
                MPI_Info_create(&info);                                                                                                       
                MPI_Info_set(info, "model", "DefaultReplaceModel()");
                MPI_Info_set(info, "generator_key", "replace_generator");
                sprintf(str, "p:%d:int,iteration_time:%f:float", p4est_state->p4est->mpisize, sim_state->work_time/sim_state->work_iters);
                //printf("Sending monitoring data: %s\n", str);
                MPI_Info_set(info, "input_pset_model_monitoring_0", str);
                p4est_dynres_set_info(p4est_state, info);
                MPI_Info_free(&info);
            }
            
            p4est_dynres_adapt(p4est_state, &terminate, &reconfigured);
        }      
        if(terminate){
            break;
        }

        make_timestamp(timestamps, EVENT_REDIST_START, p4est_state->p4est->mpisize, sim_state->current_phase);
        if(reconfigured){
            p4est_partition(p4est_state->p4est, 0, NULL);
            sim_state->work_time = 0;
            sim_state->work_iters = 0;
        }
        make_timestamp(timestamps, EVENT_REDIST_END, p4est_state->p4est->mpisize, sim_state->current_phase);

        make_timestamp(timestamps, EVENT_ADAPT_END, p4est_state->p4est->mpisize, sim_state->current_phase);

        make_timestamp(timestamps, EVENT_ITER_END, p4est_state->p4est->mpisize, sim_state->current_phase);
    }

    if(!terminate && verbosity > 0 && p4est_state->p4est->mpirank == 0){
        printf("P4est: FINISHED SIMULATION.\n");
    }
    return 0;
}


int main(int argc, char **argv) {
    
    p4est_dynres_state_t *p4est_state;
    MPI_Session session;

    timestamps = timestamp_list_new();

    /* INITIALIZE */
    make_timestamp(timestamps, EVENT_INIT_START, 0, 0);
    MPI_Session_init(MPI_INFO_NULL, MPI_ERRORS_RETURN, &session);
    p4est_state = initialize("mpi://WORLD", argc, argv);
    if(NULL == p4est_state){
        MPI_Session_finalize(&session);
        return 0;
    }
    make_timestamp(timestamps, EVENT_INIT_END, p4est_state->p4est->mpisize, 0);

    /* SIMULATE */
    simulate(p4est_state);

    /* FINALIZE */
    make_timestamp(timestamps, EVENT_FINALIZE_START, 0, 0);
    finalize(p4est_state);
    MPI_Session_finalize(&session);
    make_timestamp(timestamps, EVENT_FINALIZE_END, 0, 0);
    print_list_to_file(timestamps, outfile);

    return 0;
}
