BASE_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

MPI ?= 
P4EST ?= 
DYN_P4EST ?=
TIMESTAMPS ?=
DEBUG ?= 0
INSTALL_DIR ?= build

TS_PATH = timestamps/install
TS_TARGET=_timestamps

LIBS=-lmpi -lp4est -lsc -lp4est_dyn -ltimestamps
INCLUDE_PATHS= -I.
LIB_PATHS=

# Pick up CFLAGS
MY_CFLAGS=$(CFLAGS)
MY_CFLAGS+= -Wall
ifeq ($(DEBUG), 0)
CFLAGS+= -O3
else ifeq ($(DEBUG), 1)
CFLAGS+= -g
else ifeq ($(DEBUG), 2)
CFLAGS+= -fsanitize=address -g
endif

# Embed given paths
ifdef TIMESTAMPS
TS_PATH=$(TIMESTAMPS)
TS_TARGET=
endif
INCLUDE_PATHS+= -I$(TS_PATH)/include
LIB_PATHS+= -L$(TS_PATH)/lib -Wl,-rpath,$(TS_PATH)/lib

ifdef MPI
INCLUDE_PATHS+= -I$(MPI)/include
LIB_PATHS+= -L$(MPI)/lib -Wl,-rpath,$(MPI)/lib
endif

ifdef P4EST
INCLUDE_PATHS+= -I$(P4EST)/include
LIB_PATHS+= -L$(P4EST)/lib -Wl,-rpath,$(P4EST)/lib
endif

ifdef DYN_P4EST
INCLUDE_PATHS+= -I$(DYN_P4EST)/include
LIB_PATHS+= -L$(DYN_P4EST)/lib -Wl,-rpath,$(DYN_P4EST)/lib
endif


all: p4est_ex01 p4est_ex_dpp job_mix

p4est_ex01: examples/p4est_ex01.cpp  $(TS_TARGET)
	mkdir -p build
	mpic++ $(MY_CFLAGS) -DSUPPRESS_SOLVER_DEBUG_OUTPUT=1 -o build/p4est_ex01 $< $(INCLUDE_PATHS) $(LIB_PATHS) $(LIBS)

p4est_ex_dpp: examples/p4est_ex_dpp.cpp $(TS_TARGET)
	mkdir -p build
	mpic++ $(MY_CFLAGS) -o build/p4est_dpp $< $(INCLUDE_PATHS) $(LIB_PATHS) $(LIBS)

_timestamps:
	cd timestamps && $(MAKE) all install

install:
ifneq (${INSTALL_DIR},build)
	mkdir -p $(INSTALL_DIR)
	cp build/* $(INSTALL_DIR)
endif

clean:
	rm -rf *.o build/*

job_mix:
	echo "1,$(BASE_DIR)submissions/p4est_bs.batch," > $(BASE_DIR)/submissions/mix1.mix
	echo "10,$(BASE_DIR)submissions/p4est_gs.batch," >> $(BASE_DIR)/submissions/mix1.mix
	echo "11,$(BASE_DIR)submissions/p4est_gs.batch," >> $(BASE_DIR)/submissions/mix1.mix
	echo "12,$(BASE_DIR)submissions/p4est_bs.batch," >> $(BASE_DIR)/submissions/mix1.mix
	echo "13,$(BASE_DIR)submissions/p4est_bs.batch, {'terminate_soon': True}" >> $(BASE_DIR)/submissions/mix1.mix
