#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export OMPI_MCA_coll=^han
$SCRIPT_DIR/../build/p4est_ex01 -f 10 -t 160 -c 400 -r 6 -v 1 -o ./p4est_ex01_expand.csv >> ./p4est_expand.txt 2>&1
exit 0
