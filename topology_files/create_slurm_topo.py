import os
import yaml
import sys

def create_topology_yaml(output_file):
    # Initialize an empty dictionary to store the topology data
    topology = {'topology': {'nodes': {}}}

    # Get the SLURM_NODELIST environment variable
    slurm_nodelist = os.environ.get('SLURM_NODELIST')
    print(slurm_nodelist)
    #i01r01c01s01
    nodes = []
    for shared_prefix in slurm_nodelist.split(',i'):
        if not shared_prefix.startswith('i'):
            shared_prefix = 'i'+shared_prefix
        prefix = shared_prefix.split('s')[0]+"s"
        print(prefix)
        intervals = shared_prefix.split('s')[1].strip("[").strip("]").split(',')
        print(intervals)
        for interval in intervals:
            interval = interval.strip("]")
            node_numbers = interval.split('-')
            if len(node_numbers) == 1:
                nodes.append(prefix+node_numbers[0])
                continue

            for n in range(int(node_numbers[0]), int(node_numbers[1]) + 1):
                index = str(n) if n > 9 else "0"+str(n)
                nodes.append(prefix+index)

    # Get the SLURM_JOB_CPUS_PER_NODE environment variable
    cpus_per_node = os.environ.get('SLURM_NTASKS_PER_NODE')

    # Iterate through the nodes and cpus_per_node lists simultaneously
    for node in nodes:
        print(node)
        # Add the node to the topology dictionary
        topology['topology']['nodes'][node] = {'num_cores': int(cpus_per_node)}

    # Write the topology dictionary to a YAML file
    with open(output_file, 'w') as f:
        yaml.dump(topology, f)

if __name__ == '__main__':
    # Check if the output file argument is provided
    if len(sys.argv) != 2:
        print('Usage: python create_topology_yaml.py <output_file>')
        sys.exit(1)

    # Get the output file name from the command line argument
    output_file = sys.argv[1]

    # Call the create_topology_yaml function with the output file name
    create_topology_yaml(output_file)

