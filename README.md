# Example Application using Dynamic Resource Extensions for P4est
This repository contains an example application using the [Dynamic Resource Extensions for P4est](https://gitlab.inria.fr/dynres/applications/p4est_dyn).

## Prerequisites
- [P4est](https://github.com/cburstedde/p4est)
- [P4est_dyn](https://gitlab.inria.fr/dynres/applications/p4est_dyn)
- [Open-MPI with DPP](https://gitlab.inria.fr/dynres/dyn-procs/ompi)

## Manual Installation
The example can be compiled with make:
```
make all [MPI=/mpi/install/dir] [P4EST=/p4est/install/dir] [DYN_P4EST=/p4est/install/dir] [TIMESTAMPS=/timestamps/install/dir]
```
Specify paths to dependencies if they are not in standard locations or LIBRARY_PATH/C_INCLUDE_PATH environment.
By default, the internal version of timestamps will be used.

## Installation with DynProcsSetup
Use the scripts in the [DynProcsSetup](https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup) repo:

To install the examples and all dependencies run:
```
./envs.sh dyn_p4est_examples --install
```
To load the envirnment run:
```
source envs.sh dyn_p4est_examples --load --show --export
```


## Running a simple example without Resource Manager
```
mpirun -np 6 --host n1:2,n2:2,n3:2,n4:2 build/p4est_dpp
```

This will replace 2 running processes with 2 new processes in between phases.

## Running examples with Resource Manager using the dyn_procs_setup tests script
To run the predefined tests with DynProcsSetup:
```
./tests dyn_p4est_examples
```

## Running examples with Resource Manager manually
Requires [DynRM](https://gitlab.inria.fr/dynres/dyn-procs/dyn_rm).

The basic command to run the example is:

```
python3 run_test_dynrm.py 
    --topology_file=[path to file (relative to topology_files directory)] 
    --submission_file=[path to file (relative to submissions directory)] 
    --verbosity={0, ..., 10} // Recommended: 1 or 9
    --output_dir=[name of the directory where output should be written to (relative to output dir)] 
    --step_size={'single', 'linear', 'power_of_2'} //Step size for steepest ascend. Recommended: 'single'
```

To execute the expansion test case run:

```
python3 run_test_dynrm.py \
    --topology_file=8_node_system.yaml \
    --submission_file=p4est_expand.batch \
    --verbosity=9 \
    --output_dir=expansion_test \
    --step_size=linear 
```

To execute the job mix test case run:

```
python3 run_test_dynrm.py \
    --topology_file=8_node_system.yaml \
    --submission_file=mix1.mix \
    --verbosity=9 \
    --output_dir=job_mix_test \ 
    --step_size=linear
```