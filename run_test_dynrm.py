from dyn_rm.mca.base.system.module import *
from dyn_rm.mca.system.modules.topology import DefaultTopologyGraphCreationModule
from dyn_rm.mca.system.modules.tasks import DefaultTaskGraphCreationModule
from dyn_rm.mca.submission.modules.default import DefaultSubmissionModule
from dyn_rm.mca.base.resource_manager.module import MCAResourceManagerModule
from dyn_rm.mca.system.modules.system import PrrteSingleInstanceSystem,PrrteMultipleInstancesSystem
from dyn_rm.mca.policy.modules import Discrete_Steepest_Ascend,EasyBackfilling,FirstFitFirst

from dyn_rm.mca.base.callback.component import MCACallbackComponent
from dyn_rm.mca.base.callback.module import MCACallbackModule

from dyn_rm.mca.mca import MCAClass
from dyn_rm.util.constants import *

from os.path import join,dirname
from time import sleep

import threading

import argparse
import sys

sleep_event = threading.Event()

def wakeup(conn_name, event_name):
    sleep_event.set()
    return DYNRM_MCA_SUCCESS

def wait_for_wakeup():
    sleep_event.wait()


def run(submission_file, topo_file, output_dir, verbosity, step_size, policy, system_class):

    header =    "\n\n=======================================\n"+    \
                "   STARTING DYN_RM EXAMPLE:\n"+                    \
                "       - Topology:     "+topo_file+"\n"+           \
                "       - Submission:   "+submission_file+"\n"+     \
                "       - Policy:       "+policy.__name__+"\n"+     \
                "       - System:       "+system_class.__name__+"\n"+     \
                "       - Step_size:    "+str(step_size)+"\n"+      \
                "       - Verbosity:    "+str(verbosity)+"\n"+      \
                "       - Output dir:   "+output_dir+"\n"+          \
                "=======================================\n\n"
                
                
                

    print(header)
    sys.stdout.flush()

    # Create the resource manager for the system
    resource_manager = MCAResourceManagerModule(parent_dir = output_dir, enable_output=(output_dir!="none"), verbosity=verbosity)
    
    # Add a policy module
    resource_manager.run_service("ADD", "POLICY", "my_policy", policy, params = {'step_size': step_size})
    
    # Add a system module for the given topology description using a certian TopologyGraphCreationModule
    resource_manager.run_service("ADD", "SYSTEM", "my_system", system_class, DefaultTopologyGraphCreationModule, topo_file)

    # register a callback to be executed when all jobs have been finalized 
    user = MCAClass()
    user.register_component(MCACallbackComponent())
    user.run_component_service(MCACallbackComponent, "REQUEST", "CONNECTION", MCACallbackModule, "RM", user, resource_manager, dict())
    user.run_component_service(MCACallbackComponent, "REGISTER", "CALLBACK", MCAResourceManagerModule.ALL_TASK_GRAPHS_TERMINATED, wakeup)

    # Submit the job/job mix using the given batch script parser (TaskGraphCreationModule)
    if submission_file.endswith('.batch'):
        resource_manager.run_service("SUBMIT", "OBJECT", "my_system", DefaultSubmissionModule, submission_file, {"task_graph_creation_modules" : [DefaultTaskGraphCreationModule], "terminate_soon" : True})
    elif submission_file.endswith('.mix'):
        resource_manager.run_service("SUBMIT", "MIX", "my_system", DefaultSubmissionModule, submission_file, {"task_graph_creation_modules" : [DefaultTaskGraphCreationModule]})
    
    if verbosity > 0:
        # print current system state
        system = resource_manager.run_service("GET", "SYSTEM", "my_system")
        system.run_service("PRINT", "SYSTEM")
    
    # Wait until the callback wakes us up
    wait_for_wakeup()

    if verbosity > 0:
        # print current system state
        system.run_service("PRINT", "SYSTEM")

    # Shutdown the resource manager
    resource_manager.run_service("MCA", "SHUTDOWN")

    header =    "\n\n=======================================\n"+    \
                "   FINISHED DYN_RM EXAMPLE:\n"+                    \
                "       - Topology:     "+topo_file+"\n"+           \
                "       - Submission:   "+submission_file+"\n"+     \
                "       - Policy:       "+policy.__name__+"\n"+     \
                "       - System:       "+system_class.__name__+"\n"+     \
                "       - Step_size:    "+str(step_size)+"\n"+      \
                "       - Verbosity:    "+str(verbosity)+"\n"+      \
                "       - Output dir:   "+output_dir+"\n"+          \
                "=======================================\n\n"
    print(header)
    sys.stdout.flush()


def get_parameters():
    parser = argparse.ArgumentParser() # Add an argument
    parser.add_argument('--submission_file', type=str, required=True)
    parser.add_argument('--topology_file', type=str, required=True)
    parser.add_argument('--output_dir', type=str, required=False, default='none')
    parser.add_argument('--verbosity', type=int, required=False, default=0)
    parser.add_argument('--step_size', type=str, required=False, default='linear')
    parser.add_argument('--policy', type=str, required=False, default='Discrete_Steepest_Ascend')
    parser.add_argument('--system', type=str, required=False, default="PrrteSingleInstanceSystem")

    args = parser.parse_args()

    return  join(dirname(__file__), "submissions", args.submission_file),\
            join(dirname(__file__), "topology_files", args.topology_file),\
            join(dirname(__file__), "output", args.output_dir),\
            args.verbosity,\
            args.step_size, \
            args.policy, \
            args.system
            

if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting Micro-Benchmark'")
    print("================================================================================")
    sys.stdout.flush()
    sub_file, topo_file, output_dir, verbosity, step_size, policy, system = get_parameters()

    run(sub_file, topo_file, output_dir, verbosity, step_size, eval(policy), eval(system))

    print()
    print("================================================================================")
    print("Finished Micro-Benchmark successfully")
    print("================================================================================")
